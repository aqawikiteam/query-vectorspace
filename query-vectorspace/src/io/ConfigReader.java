package io;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;

/* Class that represents a configuration file reader */
public class ConfigReader {
	// Singleton object pointer
	private static ConfigReader reference = null;
	// Dictionary of configuration options and corresponding values
	private Map<Object, Object> configMap = new HashMap<>();

	/*
	 * Private singleton constructor accessed via factory method
	 */
	private ConfigReader(String path) {
		// Create input filestream
		InputStream inputstream = null;
		try {
			// Create configs properties object to store mapped values
			Properties configs = new Properties();
			inputstream = new FileInputStream(path); // Throws FileNotFound exception
			configs.load(inputstream); // Throws IOException
			// Map the config entries to their values
			for (Entry<Object, Object> entry : configs.entrySet()) {
				configMap.put(entry.getKey(), entry.getValue());
			}
			// Close file descriptor
			inputstream.close();
			// Without config file, parser cannot run, exit status 1
		} catch (FileNotFoundException e) {
			System.err.println("Could not find configuration file at path: " + path + ", the program will now exit.");
			System.exit(1);
		} catch (IOException e) {
			System.err.println("Unable to read configuration file, the program will now exit.");
			System.exit(1);
		}
	}

	/*
	 * Return pointer to config reader if an instance is not yet created, create a
	 * new instance otherwise
	 * 
	 * @param path to config file
	 * 
	 * @return working config instance
	 */
	public static ConfigReader getConfigReader(String path) {
		if (reference == null) {
			reference = new ConfigReader(path);
		}
		return reference;
	}

	/*
	 * Overload function without required path, only called after initial
	 * constructor call
	 */
	public static ConfigReader getConfigReader() {
		if (reference != null) {
			return reference;
		} else {
			return null;
		}
	}

	/*
	 * Return the config value from the config file
	 */
	public String getConfigValue(String configName) {
		try {
			return (String) configMap.get(configName);
		} catch (Exception e) {
			return "";
		}
	}

	/*
	 * Return the config value as a boolean
	 */
	public boolean getBool(String configName) {
		return Boolean.parseBoolean(getConfigValue(configName));
	}

	/*
	 * Return the config value as a String
	 */
	public String getStr(String configName) {
		return getConfigValue(configName);
	}

	/*
	 * Return the config value as a number
	 */
	public int getInt(String configName) {
		return Integer.parseInt(getConfigValue(configName));
	}

	/*
	 * Return the config value as a decimal number
	 */
	public Double getDouble(String configName) {
		return Double.parseDouble(getConfigValue(configName));
	}
	
	/*
	 * Return the config value as a decimal number
	 */
	public Long getLong(String configName) {
		return Long.parseLong(getConfigValue(configName));
	}

}
