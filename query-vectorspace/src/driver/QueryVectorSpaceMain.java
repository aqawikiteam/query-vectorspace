package driver;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.Scanner;
import java.util.Set;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.deeplearning4j.models.embeddings.loader.WordVectorSerializer;
import org.deeplearning4j.models.word2vec.Word2Vec;
import org.deeplearning4j.models.sequencevectors.*;
import org.deeplearning4j.models.word2vec.*;
import org.deeplearning4j.models.embeddings.loader.WordVectorSerializer;
import org.deeplearning4j.models.word2vec.Word2Vec;
import org.deeplearning4j.text.sentenceiterator.LineSentenceIterator;
import org.deeplearning4j.text.sentenceiterator.SentenceIterator;
import org.deeplearning4j.text.tokenization.tokenizer.preprocessor.CommonPreprocessor;
import org.deeplearning4j.text.tokenization.tokenizerfactory.*;
import org.nd4j.nativeblas.Nd4jCpu.static_bidirectional_rnn;

import me.xdrop.fuzzywuzzy.Applicable;
import me.xdrop.fuzzywuzzy.FuzzySearch;
import me.xdrop.fuzzywuzzy.algorithms.TokenSet;
import me.xdrop.fuzzywuzzy.algorithms.TokenSort;
import me.xdrop.fuzzywuzzy.model.ExtractedResult;
import mongojar.MongoDriver;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.bson.Document;
import org.bson.types.ObjectId;

import org.slf4j.LoggerFactory;


import com.mongodb.MongoClient;
//import com.ibm.watson.developer_cloud.natural_language_understanding.v1.model.ConceptsResult;
//import com.ibm.watson.developer_cloud.natural_language_understanding.v1.model.EntitiesResult;
//import com.ibm.watson.developer_cloud.natural_language_understanding.v1.model.KeywordsResult;
import com.mongodb.MongoClientURI;
import com.ibm.watson.natural_language_understanding.v1.model.*;
import com.ibm.watson.natural_language_understanding.v1.model.TokenResult.PartOfSpeech;


import io.ConfigReader;
import mechanism.VectorSpaceQuerying;


public class QueryVectorSpaceMain {
	
	private static final ConfigReader configReader = ConfigReader.getConfigReader("C:\\Projects\\Wiki\\configuration files\\queryvectorspace.conf");
	
	
	/**
	 * Loads the vector space into memory from the config parameters.
	 * @param config		The config file loaded into memory
	 * @return 				A vector space
	 */
	public static Word2Vec loadVectorSpace() { 
		String vecLocation = configReader.getStr("word2vec_vectorspace_location");
		Word2Vec vec = new Word2Vec.Builder().build();
		try {
			vec = WordVectorSerializer.readWord2VecModel(vecLocation);
			return vec;
		}
		catch(Exception e) {
			
			e.printStackTrace();
		}
		return null;
	}
	
	public static void main(String[] args) throws IOException {
		Word2Vec vectorspace = QueryVectorSpaceMain.loadVectorSpace();
		
		VectorSpaceQuerying.queryVectorSpace(configReader, vectorspace);
	}
}
