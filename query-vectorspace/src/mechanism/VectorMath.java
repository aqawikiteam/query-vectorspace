package mechanism;

import org.deeplearning4j.models.word2vec.Word2Vec;

public final class VectorMath {
	
	public static double cosine_similarity(double[] input1_vector, double[] input2_vector)
	{
		 double dot_product = dotProduct(input1_vector, input2_vector);
		 
	 	    double norm_a = 0.0;
		    double norm_b = 0.0;
	      //Its assumed input1_vector and input2_vector have same length (300 dimensions)
		    for (int i = 0; i < input1_vector.length; i++) 
	      {
		        norm_a += Math.pow(input1_vector[i], 2);
		        norm_b += Math.pow(input2_vector[i], 2);
		    }   
	      double cosine_sim = (dot_product / (Math.sqrt(norm_a) * Math.sqrt(norm_b)));
	      return cosine_sim;
	}

	public static double dotProduct(double[] a, double[] b) {
		double runningSum = 0;
		for (int index = 0; index < a.length; index++)
			runningSum += a[index] * b[index];
		return runningSum;
	}
	
	public static double[] addVectors(String key, Word2Vec vec) {
		String[] words = key.split(" ");
		double[] runningSum = {};
		if (vec.getWordVector(words[0]) != null) {
			runningSum = new double[vec.getWordVector(words[0]).length];// = new double[words.length];
		}
		double[] temp;
		for (String word: words) {
			try {
				temp = vec.getWordVector(word);
				for(int i = 0; i < temp.length; i++) {
					runningSum[i] += temp[i];
				}
			}
			catch (Exception e) {
				;
			}
		}
		return runningSum;
	}
	
	
	
	
	

}

