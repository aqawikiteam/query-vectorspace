package mechanism;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.NoSuchElementException;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.bson.Document;
import org.bson.types.ObjectId;

import com.mongodb.MongoClient;
//import com.ibm.watson.developer_cloud.natural_language_understanding.v1.model.ConceptsResult;
//import com.ibm.watson.developer_cloud.natural_language_understanding.v1.model.EntitiesResult;
//import com.ibm.watson.developer_cloud.natural_language_understanding.v1.model.KeywordsResult;
import com.mongodb.MongoClientURI;

import driver.QueryVectorSpaceMain;
import io.ConfigReader;
import mechanism.VectorMath;

import com.ibm.watson.natural_language_understanding.v1.model.*;
import com.ibm.watson.natural_language_understanding.v1.model.TokenResult.PartOfSpeech;

//import ca.on.gov.edu.module.watson.WatsonNLU;

import me.xdrop.fuzzywuzzy.Applicable;
import me.xdrop.fuzzywuzzy.FuzzySearch;
import me.xdrop.fuzzywuzzy.algorithms.TokenSet;
import me.xdrop.fuzzywuzzy.algorithms.TokenSort;
import me.xdrop.fuzzywuzzy.model.ExtractedResult;
import mongojar.MongoDriver;





import org.deeplearning4j.models.embeddings.loader.WordVectorSerializer;
import org.deeplearning4j.models.word2vec.Word2Vec;
import org.deeplearning4j.text.sentenceiterator.LineSentenceIterator;
import org.deeplearning4j.text.sentenceiterator.SentenceIterator;
import org.deeplearning4j.text.tokenization.tokenizer.preprocessor.CommonPreprocessor;
import org.deeplearning4j.text.tokenization.tokenizerfactory.*;
import org.nd4j.nativeblas.Nd4jCpu.static_bidirectional_rnn;
import io.ConfigReader;
import java.util.Date;
public class VectorSpaceQuerying {
	
	/**
	* 
	* Queries the vectorspace to find the related keywords of the given query, and 
	* logs the results in a text file.
	* 
	* @param  queries            the list of queries from a text file
	* @param  vec                the vectorspace
	* @param  log                the file where the results of the query will be written to
	* 
	*/
	public static void queryVectorSpace(ConfigReader configReader, Word2Vec vec) throws IOException {
		double word2vec_accuracy = configReader.getDouble("word2vec_accuracy");
		File inputQuery = new File(configReader.getStr("input_query"));
		File vspace_query_log = new File(configReader.getStr("vectorspace_query_log"));
		
		String query;
		Scanner sc = new Scanner(inputQuery);
		
		query = sc.nextLine();
		sc.close();
		
		
		Set<String> wordSet = new HashSet<String>();
		if ((query.split(" ")).length > 1) {
			for (String component : query.split(" ")) {
				wordSet.addAll(vec.similarWordsInVocabTo(component.toLowerCase(), word2vec_accuracy));
			}
		} 
		else 
			wordSet.addAll(vec.similarWordsInVocabTo(query.toLowerCase(), word2vec_accuracy));
	
		
		List<String> tempKeys = new ArrayList<String>(wordSet);
		LinkedHashMap<String, Object> wordToScore = new LinkedHashMap<String, Object>();
		wordToScore.putAll(scoreList(vec, query.toLowerCase(), tempKeys, 0));
		
		Date d = new Date();
		
		FileWriter fW = new FileWriter(vspace_query_log, true);
		fW.write("***************************************************************\n");
		fW.write("Date of logging: "+d+"\n");
		fW.write("Query: "+query+"\n");
		for (String key : wordToScore.keySet()) {
			fW.write(key+", "+wordToScore.get(key)+"\n");
		}
		fW.write("***************************************************************\n");
		
		fW.close();
	}
	
	public static LinkedHashMap<String, Object> scoreList(Word2Vec vec, String originalKey, List<String> allKeys, double limit) {
//		HashMap<String, Double> scoredList = new HashMap<String, Double>();
		TreeMap<Double, String> scoredList = new TreeMap<Double, String>();

		double score = 0;
		//int matchedKeywords = 0;
		// loop through related keys and get the score for each word
		// compared to original keyword
		for(String key: allKeys) {
//			score = vec.similarity(originalKey, key);
			//System.out.println(originalKey);
			double [] originalKeyVectorized = VectorMath.addVectors(originalKey, vec);
			try {
				score = VectorMath.cosine_similarity(originalKeyVectorized,vec.getWordVector(key));
			}
			catch(Exception e) {
				//Util.logInfo("Error while calculating cosine similarity (Possible error with dl4j jars)");
				//Util.logInfo("Description of error: " + e.getLocalizedMessage());
				;
			}
			score = score * Math.pow(10, 14);
			score = Math.round(score);
			score = score * Math.pow(10, -14);

			if(!Double.isNaN(score) && score != 1 && score > limit) {  //"score != 1" to prevent the original keyword itself from appearing
				//matchedKeywords = matchedKeywords + 1;
				try {
					scoredList.put(score, key);
				}
				catch (Exception e) {
					scoredList.put(score+0.00000001, key);
					e.printStackTrace();
				}
			}
		}

		LinkedHashMap<String, Object> orderedList = null;
		
		// check if word is simple or complex and apply
		// respective threshold
		if (originalKey.split(" ").length > 1) {
			System.out.println("complex");
			orderedList = apply_threshold(scoredList, "complex");
		} else {
			System.out.println("simple");
			orderedList = apply_threshold(scoredList, "simple");
		}
		
		System.out.println(originalKey + ": " + orderedList);
		
		return orderedList;
	}
	
	private static LinkedHashMap<String, Object> apply_threshold(TreeMap<Double, String> scoredList, String keywordType) {
		ConfigReader configReader = ConfigReader.getConfigReader("C:\\Projects\\Wiki\\configuration files\\lessonslearnedwiki.conf");
		LinkedHashMap<String, Object> orderedList = new LinkedHashMap<String, Object>();

		double minCosineSimilarity = 0.0;
		int minKeywords = 0;
		// get respective threshold from config file
		if (keywordType.equals("simple")) {
			minCosineSimilarity = configReader.getDouble("word2vec_simple_minimum_cosine_similarity");
			minKeywords = configReader.getInt("word2vec_simple_top");
		} else if (keywordType.equals("complex")){
			minCosineSimilarity = configReader.getDouble("word2vec_complex_minimum_cosine_similarity");
			minKeywords = configReader.getInt("word2vec_complex_top");
		}
		
		int numberOfMatches = 0;
		// loop through scoredList finding keywords that meet threshold and ordering them in a list
		for(int i = scoredList.size()-1; i >= 0; i--) {
			if ((double)scoredList.keySet().toArray()[i] > minCosineSimilarity || minKeywords > numberOfMatches) {
				orderedList.put(scoredList.get(scoredList.keySet().toArray()[i]),scoredList.keySet().toArray()[i]);
				numberOfMatches++;
			} else {
				break;
			}
		}

		return orderedList;
	}

}

